
//Aggregation to extract users by id 

// Stages:

// 	1. '$unwind' each user can have diferrents roles, we should to unfold the role array and create one row by roleID.
// 	2.  '$group' groups the rows by rol and create the array users that contains all users email with the current role
// 	3. '$project' format the output to get properties role and the array of users so it remove the _id property.



db.users.aggregate([
    {
        $unwind: "$roles"
      },
      {
        $group: {
          _id: "$roles",
          users: { $push: "$email" }
        }
      },
      {
        $project: {
          role: "$_id",
          users: 1,
          _id: -1
        }
      }
]);