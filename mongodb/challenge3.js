
const date = new Date();
const documentID = ObjectId("5cd96d3ed5d3e20029627d4a");

// update last_connection_date
db.users.updateOne(
  { _id: documentID },
  { $set: { last_connection_date: currentDate } }
);

// update role adding a new one
db.users.updateOne(
  { _id: documentID },
  { $addToSet: { roles: "5cd96d3ed5d3e20029627d4a" } }
);

// update nested property
db.users.updateOne(
  { _id: documentID, "addresses.zip": "75001" },
  { $set: { "addresses.$.city": "Paris 1" } }
);