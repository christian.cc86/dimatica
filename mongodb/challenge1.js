const email = "user1@dimatica.com";
const dateLimit = new Date();
dateLimit.setMonth(dateLimit.getMonth() - 6);

const q = {
   // exact email address search
   email: email ,
   // Date sorting
   last_connection_date: { $gte: sixMonthsAgo },
};

const rs = db.users.find(q);



// Improving performance adding indexes:
// always try to avoid the use of regular expressions, in this case we do the exact match find.

// creation of case insensitive index of email
db.users.createIndex(
    { email: 1},
    { unique: true },
        { collation: {
                locale : 'en',
                strength : 1
        
} });

//creation of last_connection_date to sort dates to improve performance using filters
db.users.createIndex({ last_connection_date: -1 });