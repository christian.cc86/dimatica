import got from 'got';

// challenge 1

// Call web service and return count user, (got is library to call url)
// async function getCountUsers() {
//   return { total: await got.get('https://my-webservice.moveecar.com/users/count') };
// }

// Add total from service with 20
// async function computeResult() {
//   const result = getCountUsers();
//   return result.total + 20;
// }

// issues:
// - Is miissing got library import
// - getCountUsers returns  the entire response object to the property total.
// - getCountUsers Is not Parsing the response body object
// - getCountUsers is not handling exceptions comming from the getCountUsers function
// - getCountUsers is not handling the status code returned
// - computeResult() function is calling getCountUsers function without resolving the promise
// - computeResult() function is assuming that get the happy path where we found the result object
//   with property total.

// proposal:

// eslint-disable-next-line no-console
const logger = console.error;
const countUsersURL = 'https://my-webservice.moveecar.com/users/count';
const totalVehiclesURL = 'https://my-webservice.moveecar.com/vehicles/total';

async function get(url) {
  try {
    const rs = await got.get(url);
    if (rs.statusCode === 200) {
      return JSON.parse(rs.body);
    }

    throw new Error(`invalid response: ${rs.statusCode}`);
  } catch (e) {
    logger({ status: e.code });
    throw new Error(`error on request with status code: ${e.code}`);
  }
}

async function getCountUsers() {
  try {
    return { total: (await get(countUsersURL)).count };
  } catch (e) {
    logger({ status: e });
    throw new Error(`${e.message}`);
  }
}

// Add total from service with 20
async function computeResult() {
  try {
    const result = await getCountUsers();
    return result.total + 20;
  } catch (e) {
    logger({ message: e.message });
    throw new Error('Invalid user count');
  }
}

computeResult().then((rs) => logger(rs)).catch((e) => logger(e.message));

// Challenge 2.

// Call web service and return total vehicles, (got is library to call url)
// async function getTotalVehicles() {
//   return await got.get('https://my-webservice.moveecar.com/vehicles/total');
// }

// function getPlurial() {
//   let total;
//   getTotalVehicles().then((r) => total = r);
//   if (total <= 0) {
//     return 'none';
//   }
//   if (total <= 10) {
//     return 'few';
//   }
//   return 'many';
// }

// issues:
// - getTotalVehicles() returning the entire response object.
// - getTotalVehicles() is redundant await on return statement,
//   is not necessary because in returns function wait for promise resolution.
// - getTotalVehicles() Unhandled exceptions validate the response object and the request
// - getTotalVehicles returning unparsed response body object
// - getPlurial() contains unhandled exceptions.
// - getPlurial() assumes that the returned object from getTotalVehicles is a number
// - getPlurial() promise resolution Arrow function should not return assignment.
// - getPlurial() promise aproach will fail because total will be asigned when it returns 'many'.

async function getTotalVehicles() {
  try {
    return await get(totalVehiclesURL);
  } catch (e) {
    logger({ status: e });
    throw new Error(`${e.message}`);
  }
}

function getPlurial() {
  return getTotalVehicles()
    .then((r) => {
      const { total } = r;

      if (total <= 0) {
        return 'none';
      } if (total <= 10) {
        return 'few';
      }

      return 'many';
    })
    .catch((e) => {
      logger({ message: e.message });

      throw new Error(`error getting total vehicles value ${e.message}`);
    });
}

// execition of getPluriar would be simplified using async/await and the output .
getPlurial()
  .then((r) => {
    logger(r);
  })
  .catch((e) => {
    logger(e.message);
  });
