// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { type data, errorNullString } from './constants'

export function getCapitalizeFirstWord (name: string): string {
  if (name == null) {
    throw new Error(errorNullString)
  }

  // also the linter validation fails
  // validation redundant becauyse again it validates if the string is null or undefined
  // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
  if (!name) {
    return name
  }

  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ')
}

// uncoment for testing purposes
// happy path
// console.log('happy path', getCapitalizeFirstWord('christian'))
// second option on string lenght is 1
// console.log('string length 1 ', getCapitalizeFirstWord('c'))

// console.log('empty string case', getCapitalizeFirstWord(''))

// enforce throw exception
// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
// const str: data = {} as data
// getCapitalizeFirstWord(str.word)
