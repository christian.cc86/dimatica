import { errorNullString, type data } from './constants'
import { getCapitalizeFirstWord } from './index'
import { expect, describe, it } from '@jest/globals'

describe('getCapitalizeFirstWord tests', () => {
  it('1. should return exception on undefined string', () => {
    try {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const str: data = {} as data
      getCapitalizeFirstWord(str.word)
    } catch (err: any) {
      expect(err.message).toBe(errorNullString)
    }
  })

  it('2. should return empty string', () => {
    expect(getCapitalizeFirstWord('')).toBe('')
  })

  it('3. should capitalize first word', () => {
    expect(getCapitalizeFirstWord('christian cardenas')).toBe('Christian Cardenas')
  })

  it('4. should return lowercase string', () => {
    expect(getCapitalizeFirstWord('c')).toEqual('c')
  })
})
