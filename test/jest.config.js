/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  rootDir: './',
  collectCoverageFrom: ['<rootDir>/src/*.ts', '!<rootDir>/src/constants.ts'],
  testMatch: ['<rootDir>/src/*.spec.ts']
}
