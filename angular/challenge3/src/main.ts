import { CommonModule } from '@angular/common';
import { Component, Output, EventEmitter} from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { AbstractControl, FormBuilder, FormGroup, ReactiveFormsModule, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  standalone: true,
  imports:[ReactiveFormsModule, CommonModule],
  providers:[],
  selector: 'app-users-form',
  template: `
    <form [formGroup]="userForm" (ngSubmit)="doSubmit()" >
    <input type="text" placeholder="email" formControlName="email">
    <input type="text" placeholder="name" formControlName="name">
    <input type="date" placeholder="birthday" formControlName="birthday">
     <input type="number" placeholder="zip" formControlName="zip">
    <input type="text" placeholder="city" formControlName="city">
    <button type="submit" [disabled]="userForm.invalid">Submit</button>
  </form>
  `,

})

export class AppUserForm {
  @Output()
  event = new EventEmitter<{ 
    email: string;
    name: string;
    birthday: Date;
    address: { 
      zip: number;
      city: string;
    };
  }>;

  userForm: FormGroup;

  
  constructor(
    private fb: FormBuilder
  ) {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: ['', [ this.validateBirthday()]],
      zip: ['', [Validators.required]],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z\s]+$/)]],
    });

  }

  validateBirthday():ValidatorFn {
    return (control:AbstractControl) : { [key: string]: any } | null => {
      if (!control.value) return null
      const date = new Date().toISOString().split('T')[0]
      return Date.parse(control.value) >(Date.parse(date)-86400000) ? { futureDate:true }: null ;
    }    
  }


  doSubmit(): void {
    if (this.userForm.valid) {
      const formData = {
        email: this.userForm.value.email,
        name: this.userForm.value.name,
        birthday: this.userForm.value.birthday,
        address: {
          zip: this.userForm.value.zip,
          city: this.userForm.value.city,
        },
      };
      this.event.emit(formData);
    }
  }

  }


bootstrapApplication(AppUserForm);

