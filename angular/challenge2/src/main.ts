import { CommonModule } from '@angular/common';
import { Component, Input, ChangeDetectionStrategy, Pipe, PipeTransform } from '@angular/core';
import { userData } from './userModel';
import { bootstrapApplication } from '@angular/platform-browser';


//Improvements:
// 1. Implementation of  pipe instead to do the transformation or add methods in the template that this is considering a bad practice also using of pipes catches the values.
// 2. Improvements in the transformation logic using a basic regular expression and first lower case the entire string and later capitalize it.
// 3. Improve performance of ngfor implementing  trackBy directive (that tells Angular how to identify each element in the list).
// 4. Change detection strategy to OnPush to prevent unnecesary validations.
//
// now we can handle large amounts of data in the user list compmponent/table. 


@Pipe({
  name: 'capitalize',
  standalone: true
})

export class CapitalizePipe implements PipeTransform {
    transform(value: string): string {
    if (!value) return value;
    return value.toLowerCase().replace(/\b\w/g, char => char.toUpperCase());
  }
}

@Component({
  standalone: true,
  imports:[CommonModule,CapitalizePipe],
  providers:[],
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users; trackBy: trackByUserName">
      {{ user.name | capitalize }}
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AppUsers {
  
  @Input() users: { name: string }[] = userData;

  trackByUserName(index: number, user: { name: string }): string {
    return user.name;
  }
}
bootstrapApplication(AppUsers);

