
import { Component, OnInit } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import {
  of,
  Subject,
} from 'rxjs';
import { catchError, debounceTime, switchMap } from 'rxjs/operators';
import { UserService } from './user.service';
import { users } from './userModel';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


// Problems found:
//  - concat of "query" and "querySubject" could never do de subscription.
// 	- concatMap and timer with pipe provides invalid arguments and will not work. to avoid this problem we can use switchMap to enforce a new observable every time and provide the correct argument to concatMap.
//  - if we solve the previos issues makes no sense to call the user service findUsers method every 60 seconds so the user experience will be impacted.

// Proposals:

// 	1. Use of pipe with switchMap to ensure to use the lastest input value.
//  2. Change the way of consume findUsers method from search every 60 seconds from last input value to find users at the end of user input interaction.
// 	3. Input is forced to wait 500 milliseconds in order to prevent resolve de observable on every change of the input.
// 	3. Added error handling.
// 	5. Implementation of ngOnDestroy to avoid memory leaks issues.

	
@Component({
  standalone: true,
  imports: [CommonModule, FormsModule],
  providers: [UserService],
  selector: 'app-users',
  template: `
  Input
    <input type="text" [(ngModel)]="query"  (ngModelChange)="querySubject.next($event)">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `,
})
export class AppUsers implements OnInit {
  query = '';
  querySubject = new Subject<string>();

  users: users[] = [];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.querySubject
      .asObservable()
      .pipe(
        debounceTime(500),
        switchMap((q) => of(this.userService.findUsers(q)).pipe(catchError(() =>[])))
        )
      .subscribe({
        next: (res: any) => (this.users = res),
        error: (error) => {
          console.error('error:', error);
        }
      });
  }

  ngOnDestroy(): void {
    if (this.querySubject) {
      this.querySubject.unsubscribe();
    }
  }
}

bootstrapApplication(AppUsers);
